import { REST, Routes, Client, GatewayIntentBits } from 'discord.js';
import stfuMessages from './stfuMessages.js';
import { CronJob } from 'cron';
import 'dotenv/config';

const client = new Client({ intents: [GatewayIntentBits.Guilds, GatewayIntentBits.GuildMessages, GatewayIntentBits.MessageContent] });
const { TOKEN, BOT_CLIENT_ID } = process.env;

const cooldownedUsers = [];

const commands = [
  {
    name: 'tg',
    description: 'Appelle le pote franul à l\'aide pour lui dire de la fermer',
    options: [
      {
        name: 'target',
        description: 'la target',
        required: true,
        type: 6,
        autocomplete: true,
      }
    ]
  }
];

const job = new CronJob(
  '0 0 0 * * *',
  () => {
    while (cooldownedUsers.length > 0) {
      cooldownedUsers.pop();
    }
  },
  null,
  true,
  'Europe/Paris'
);

const rest = new REST({ version: '10' }).setToken(TOKEN);
try {
  await rest.put(Routes.applicationCommands(BOT_CLIENT_ID), { body: commands });
  console.log('Ready!');
} catch (error) {
  console.error(error);
}

client.on("messageCreate", (message) => {
  if (message.content.toLowerCase().search('quoi') >= 0 && message.author.id !== '266280003606544405' && !message.author.bot) return message.reply('FEUR !');
  return false;
});

client.on('interactionCreate', async (interaction) => {
  const { id: userId, username: userName } = interaction.user;
  const { id: targetId, username: targetName } = interaction.options._hoistedOptions[0].user;

  if (!interaction.isChatInputCommand()) return;

  if (interaction.commandName === 'tg') {
    if (cooldownedUsers.includes(userId)) return await interaction.reply({ content: 'T\'as déjà utilisé ton /tg journalier', ephemeral: true })
    else if (userId !== '266280003606544405') cooldownedUsers.push(userId);

    if (targetId === '266280003606544405') {
      await interaction.reply({ content: 'Aie la boulette', ephemeral: true });
      return client.channels.cache.get(interaction.channelId).send(`Ce gros gitan essaye de salir notre maître, honte à toi <@${userId}> !`)
    }

    if (targetId === '280759817344122890' && userId !== '266280003606544405') {
      await interaction.reply({ content: 'C\'est pas sympa ça', ephemeral: true });
      return client.channels.cache.get(interaction.channelId).send(`Dis donc <@${userId}>, c'est pas très gentil de casser du sucre sur le dos du Yosh. Bouffe ses oeufs, ça te fera les dents !`)
    }

    await interaction.reply({ content: 'Et paf le chien', ephemeral: true });
    client.channels.cache.get(interaction.channelId).send(await stfuMessages(targetId, userId))
    console.log(`/tg from ${userId}(${userName}) to ${targetId}(${targetName})`);

    return;
  }
});

client.on('ready', () => job.start())

client.login(TOKEN);
