import axios from 'axios';

export default async (to, from) => {
    const { data } = await axios.get('https://opensheet.elk.sh/1v_vlbsYi89sGUdezQMyu3JoVXY--May72yrGnSoDx78/1')
    const formattedData = data.map(message => (message['Phrase de trash'].replaceAll('[]', `<@${to}>`).replaceAll('{}', `<@${from}>`)));
    return formattedData[Math.floor(Math.random()*formattedData.length)];
}
